FROM nvidia/cuda:11.2.0-base

WORKDIR /app

COPY . /app

RUN apt-get update  && \
    apt-get -y upgrade  && \
    apt-get -y install wget  && \
    wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh && \
    chmod +x Miniconda3-latest-Linux-x86_64.sh && \
    ./Miniconda3-latest-Linux-x86_64.sh -b -p /opt/conda && \
    ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
    echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
    echo "conda activate base" >> ~/.bashrc && \
    find /opt/conda/ -follow -type f -name '*.a' -delete && \
    find /opt/conda/ -follow -type f -name '*.js.map' -delete && \
    /opt/conda/bin/conda clean -afy && \
    /opt/conda/bin/conda config --add channels conda-forge && \
    /opt/conda/bin/conda config --add channels talley && \
    /opt/conda/bin/conda create -n decon_env pycudadecon

SHELL ["/opt/conda/bin/conda", "run", "-n", "decon_env", "/bin/bash", "-c"]

RUN /opt/conda/bin/conda install -n decon_env pycudadecon scikit-image
    
ENV PATH="/opt/conda/bin/:$PATH"   
ENV PATH="/app/:$PATH" 

CMD ["bash"]
